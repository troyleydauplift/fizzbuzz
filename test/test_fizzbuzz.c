#ifdef TEST

#include "unity.h"

#include "fizzbuzz.h"
#include "mock_led.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_fizzbuzz_sets_led_to_green_when_given_3(void)
{
    set_led_color_Expect(LED_GREEN);

    fizzbuzz(3);
}

void test_fizzbuzz_sets_led_to_red_when_given_5(void)
{
    set_led_color_Expect(LED_RED);

    fizzbuzz(5);
}

void test_fizzbuzz_sets_led_to_red_when_given_multiple_of_5(void)
{
    set_led_color_Expect(LED_RED);

    fizzbuzz(10);
}

void test_fizzbuzz_sets_led_to_off_when_given_non_multiple_value(void)
{
    set_led_color_Expect(LED_OFF);

    fizzbuzz(2);
}

void test_fizzbuzz_sets_led_to_green_when_given_multiple_of_3(void)
{
    set_led_color_Expect(LED_GREEN);

    fizzbuzz(9);
}

void test_fizzbuzz_sets_led_to_amber_when_given_multiple_of_3_and_5(void)
{
    set_led_color_Expect(LED_AMBER);

    fizzbuzz(15);
}

#endif // TEST
