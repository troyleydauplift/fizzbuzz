#include "fizzbuzz.h"
#include "led.h"

void fizzbuzz(uint8_t value)
{
    if((value % 15) == 0)
    {
        set_led_color(LED_AMBER);
    }
    else if((value % 5) == 0)
    {
        set_led_color(LED_RED);
    }
    else if((value % 3) == 0)
    {
        set_led_color(LED_GREEN);
    }
    else
    {
        set_led_color(LED_OFF);
    }
}