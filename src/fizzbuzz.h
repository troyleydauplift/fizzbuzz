#ifndef FIZZBUZZ_H
#define FIZZBUZZ_H

#include <stdint.h>

void fizzbuzz(uint8_t value);

#endif // FIZZBUZZ_H
