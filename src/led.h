#ifndef LED_H
#define LED_H

typedef enum {
    LED_OFF,
    LED_GREEN,
    LED_RED,
    LED_AMBER
} color_t;

void set_led_color(color_t color);

#endif // LED_H
